//
//  AppDelegate.h
//  TeenaSample
//
//  Created by Heyward on 9/12/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

