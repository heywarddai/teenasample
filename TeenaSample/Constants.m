//
//  Constants.m
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import "Constants.h"

//#warning To run this sample correctly, you must set an appropriate constants.
AWSRegionType const CognitoRegionType = AWSRegionUSEast1; // e.g. AWSRegionUSEast1
AWSRegionType const DefaultServiceRegionType = AWSRegionUSWest1; // e.g. AWSRegionUSEast1
NSString *const CognitoIdentityPoolId = @"us-east-1:ecb7e9cd-6108-46bd-b806-4f63240480fd";
NSString *const BucketName = @"teenasample";