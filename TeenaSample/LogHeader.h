//
//  LogHeader.h
//  TeenaSample
//
//  Created by Heyward on 9/12/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#ifndef TeenaSample_LogHeader_h
    #define TeenaSample_LogHeader_h
    #define FNLog() DBLog(@"%@: %@", [self class], NSStringFromSelector(_cmd))

    #ifdef DEBUG

        #define DBLog(...) NSLog(__VA_ARGS__)

    #else

        #define DBLog(...)

    #endif

#endif
