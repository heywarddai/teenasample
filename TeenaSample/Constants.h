//
//  Constants.h
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSCore.h>

FOUNDATION_EXPORT AWSRegionType const CognitoRegionType;
FOUNDATION_EXPORT AWSRegionType const DefaultServiceRegionType;
FOUNDATION_EXPORT NSString *const CognitoIdentityPoolId;
FOUNDATION_EXPORT NSString *const BucketName;
