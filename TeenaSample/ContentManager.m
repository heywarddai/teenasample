//
//  ContentManager.m
//  TeenaSample
//
//  Created by Heyward on 9/12/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import "ContentManager.h"

#import "Constants.h"
#import <AWSS3/AWSS3.h>

@interface ContentManager ()
@property (nonatomic, strong)AWSS3TransferManager *transferManager;
@end

@implementation ContentManager

#pragma mark - Init

+ (ContentManager *)sharedInstance {
    FNLog();
    
    static ContentManager *sharedInstance = nil;
    static dispatch_once_t token;
    if (!sharedInstance) {
        dispatch_once(&token, ^{
            sharedInstance = [[super alloc] init];
        });
    }
    return sharedInstance;
}

- (instancetype) init {
    FNLog();
    
    self = [super init];
    if (self) {
        
        // Contents is empty in init...
        self.contents = [NSArray array];
        
        self.transferManager = [AWSS3TransferManager defaultS3TransferManager];
    }
    return self;
}

#pragma mark - Public

// This gets a list of available objects from the server
- (void)fetchContentsWithCompletion:(void (^)(BOOL))completion {
    FNLog();
    
    AWSS3 *s3 = [AWSS3 defaultS3];
    
    AWSS3ListObjectsRequest *listObjectsRequest = [AWSS3ListObjectsRequest new];
    listObjectsRequest.bucket = BucketName;
    [[s3 listObjects:listObjectsRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"listObjects failed: [%@]", task.error);
            if (completion) {
                completion(NO);
            }
        } else {
            AWSS3ListObjectsOutput *listObjectsOutput = task.result;
            NSArray *rawContents = listObjectsOutput.contents;
            NSMutableArray *contents = [self makeModelsFromRawContents:rawContents];
            self.contents = contents;
            if (completion) {
                completion(YES);
            }
        }
        return nil;
    }];
}

- (void)getFullModel:(Model *)model completion:(void (^)(BOOL, NSError *))completion {

    NSString *downloadingFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt", model.title]];
    NSURL *downloadingFileURL = [NSURL fileURLWithPath:downloadingFilePath];

    AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
    downloadRequest.bucket = BucketName;
    downloadRequest.key = model.title;
    [downloadRequest setDownloadingFileURL:downloadingFileURL];

    [[self.transferManager download:downloadRequest] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"%@", task.error.localizedDescription);
            if (completion) {
                completion(NO, task.error);
            }
        } else {
            NSString *text = [NSString stringWithContentsOfURL:downloadingFileURL encoding:NSUTF8StringEncoding error:nil];
            [model setText:text];
            if (completion) {
                completion(YES, nil);
            }
            
        }
        
        [[NSFileManager defaultManager] removeItemAtURL:downloadingFileURL error:nil];
        return nil;
    }];
}

- (void)createModel:(Model *)model completion:(void(^)(BOOL success, NSError *error))completion {
    FNLog();
    
    BOOL isValid = [self checkModel:model];
    
    NSError *error;

    if (isValid) {
        // Create txt file
        NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt", model.title]];
        [model.text writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
        uploadRequest.body = [NSURL fileURLWithPath:filePath];
        uploadRequest.key = model.title;
        uploadRequest.bucket = BucketName;
        
        [[self.transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
            if (task.error) {
                if (completion) {
                    completion(NO, task.error);
                }
            }
            
            if (task.result) {
                if (completion) {
                    completion(YES, nil);
                }
            }
            
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            
            return nil;
        }];


    } else {
        
        error = [NSError errorWithDomain:@"S3 Error" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Title and text must not be blank"}];
        
        if (completion) {
            completion(NO, error);

        }
    }
}

- (void)updateModel:(Model *)model withTitle:(NSString *)title text:(NSString *)text completion:(void(^)(BOOL success, NSError *error))completion {
    FNLog();
    
    BOOL isValid = [self checkModel:model];
    
    if (isValid) {
        
        // Is there a better way to update rather than deleting and uploading again?
        
        AWSS3 *s3 = [AWSS3 defaultS3];
        AWSS3DeleteObjectRequest *deleteRequest = [AWSS3DeleteObjectRequest new];
        deleteRequest.bucket = BucketName;
        deleteRequest.key = model.title;
        [[[s3 deleteObject:deleteRequest] continueWithBlock:^id(AWSTask *task) {
            if(task.error == nil){
                
                [model setTitle:title];
                [model setText:text];
                [self createModel:model completion:completion];
                
            }else{
                
                if(task.error.code != AWSS3TransferManagerErrorCancelled && task.error.code != AWSS3TransferManagerErrorPaused){
                    NSLog(@"%s Error: [%@]",__PRETTY_FUNCTION__, task.error);
                }
                
                if (completion) {
                    completion(NO, task.error);
                    
                }
            }
            return nil;
        }] waitUntilFinished];
        
    } else {
        
        NSError *error = [NSError errorWithDomain:@"S3 Error" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Title and text must not be blank"}];
        
        if (completion) {
            completion(NO, error);
            
        }
    }
}

#pragma mark - Private

- (BOOL)checkModel:(Model *)model {
    FNLog();
    
    return model.text.length > 0 && model.title.length > 0;
}

- (NSMutableArray *)makeModelsFromRawContents:(NSArray *)rawContents {
    FNLog();
    
    NSMutableArray *newContents = [NSMutableArray array];
    
    for (AWSS3Object *object in rawContents) {
        Model *newModel = [[Model alloc] init];
        [newModel setTitle:object.key];
        [newContents addObject:newModel];
    }
    
    return newContents;
}

@end
