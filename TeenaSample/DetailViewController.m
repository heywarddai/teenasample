//
//  DetailViewController.m
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#define textFieldHeight 44.0f
#define textFieldMargin 20.0f

#import "DetailViewController.h"

#import "ContentManager.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    FNLog();
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated {
    FNLog();
    
    [self.titleField becomeFirstResponder];
    
    if (self.model) {
        [self.titleField setText:self.model.title];
        [self.textView setText:self.model.text];
        
        [self.spinner startAnimating];
        [[ContentManager sharedInstance] getFullModel:self.model completion:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }]];
                    [self presentViewController:alertController animated:YES completion:nil];
                } else {
                    [self.titleField setText:self.model.title];
                    [self.textView setText:self.model.text];
                    
                }
                
                [self.spinner stopAnimating];
                
            });
        }];
        
    } else {
        [self.titleField setText:nil];
        [self.textView setText:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonTapped:(id)sender {
    FNLog();
    
    if (self.model) {
        
        [[ContentManager sharedInstance] updateModel:self.model withTitle:self.titleField.text text:[self.textView text] completion:^(BOOL success, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [self showAlertWithMessage:[error localizedDescription]];
                }

            });
        }];
        
    } else {
        Model *newModel = [[Model alloc] init];
        [newModel setTitle:self.titleField.text];
        [newModel setText:[self.textView text]];
        [[ContentManager sharedInstance] createModel:newModel completion:^(BOOL success, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    [self showAlertWithMessage:[error localizedDescription]];
                }
            });
        }];
    }
}

- (void)showAlertWithMessage:(NSString *)message {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
