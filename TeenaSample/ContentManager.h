//
//  ContentManager.h
//  TeenaSample
//
//  Created by Heyward on 9/12/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Model.h"

@interface ContentManager : NSObject

// Returns an mutable array of Model objects
@property (nonatomic, strong) NSArray *contents;

+ (instancetype)sharedInstance;

// Fetches contents from server
- (void)fetchContentsWithCompletion:(void(^)(BOOL success))completion;

- (void)getFullModel:(Model *)model completion:(void(^)(BOOL success, NSError *error))completion;

// Create a new model and upload to server
- (void)createModel:(Model *)model completion:(void(^)(BOOL success, NSError *error))completion;

// Update an existing model
- (void)updateModel:(Model *)model withTitle:(NSString *)title text:(NSString *)text completion:(void(^)(BOOL success, NSError *error))completion;

@end
