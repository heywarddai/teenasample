//
//  DetailViewController.h
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Model.h"

@interface DetailViewController : UIViewController
@property (nonatomic, strong) Model *model;
@end
