//
//  ListViewController.m
//  TeenaSample
//
//  Created by Heyward on 9/12/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#define statusBarHeight 20.0f

#import "ListViewController.h"
#import "DetailViewController.h"

#import "ContentManager.h"

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation ListViewController

- (void)viewDidLoad {
    FNLog();
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self.view addSubview:self.tableView];

}

- (void)viewWillAppear:(BOOL)animated {
    FNLog();
    
    [super viewWillAppear:animated];
    
    [self downloadContents];
}

#pragma mark - Private

- (IBAction)onRefreshButtonTapped:(id)sender {
    FNLog();
    
    [self downloadContents];
}

- (void)downloadContents {
    FNLog();
    
    [[ContentManager sharedInstance] fetchContentsWithCompletion:^(BOOL success) {
        if (success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
            
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    FNLog();
    
    NSUInteger count = [[[ContentManager sharedInstance] contents] count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FNLog();
    
    Model *model = [[[ContentManager sharedInstance] contents] objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    [cell.textLabel setText:model.title];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FNLog();
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Model *model = [[[ContentManager sharedInstance] contents] objectAtIndex:indexPath.row];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    DetailViewController *detailViewController = [storyBoard instantiateViewControllerWithIdentifier:@"detailViewController"];
    [detailViewController setModel:model];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - Clean up

- (void)didReceiveMemoryWarning {
    FNLog();
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
