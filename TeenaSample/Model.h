//
//  Model.h
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

// We are just going to pretend the model only has one property called title.

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *text;

- (instancetype)init;
@end
