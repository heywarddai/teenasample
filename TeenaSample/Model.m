//
//  Model.m
//  TeenaSample
//
//  Created by Heyward on 9/14/15.
//  Copyright (c) 2015 HeywardDai. All rights reserved.
//

#import "Model.h"

@implementation Model

- (instancetype)init {
    FNLog();
    
    self = [super init];
    if (self) {
        _title = @"";
        _text = @"";
    }
    return self;
}

@end
